# C++ Socket Fun

## Initial Goal

Implement a server in C++ that listens on a specific port-- use netcat to send basic messages.

## Running

1. Compile program: `g++ src/main.cpp`
2. Start server: `./a.out`
3. Connect to server (using netcat): `nc 127.0.0.1 8080` (8080 is the hardcoded port)

In the netcat window you should see a message from the server. If you type characters and press enter, you will see the output in the server binary output.
