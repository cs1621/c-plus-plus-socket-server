/*
 * C++ Socket Server
 * See README.md for more information
 *
 * Author: Samuel Lim
 */
#include <iostream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>

using namespace std;

const int PORT = 8080;

int main()
{
    cout << "Starting..." << endl;
    // create a socket
    cout << "[+] Creating socket..." << endl;
    int server_socket = socket(AF_INET, SOCK_STREAM, 0);
    cout << "[+] Done" << endl;

    // specify the server address and port
    struct sockaddr_in server_address;
    // AF_INET is defined in POSIX It's an address family
    // Supports IPv4 (and 6?)
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = INADDR_ANY;
    server_address.sin_port = htons(PORT);

    // bind the socket to the server address and port
    cout << "[+] Binding socket...." << endl;
    int bind_results = bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address));
    if (bind_results == 0)
    {
        cout << "[+] Done" << endl;
    }

    // listen for incoming connections
    cout << "[+] Listening..." << endl;
    listen(server_socket, 3);
    cout << "[+] Done" << endl;

    // accept incoming connections
    int client_socket;
    struct sockaddr_in client_address;
    socklen_t addrlen = sizeof(client_address);
    cout << "[+] Listening for incoming connections..." << endl;
    client_socket = accept(server_socket, (struct sockaddr *)&client_address, &addrlen);

    // send data to the client
    const char *message = "You have connected";
    send(client_socket, message, strlen(message), 0);

    // receive data from the client
    char buffer[1024] = {0};
    read(client_socket, buffer, 1024);
    cout << "Client message: " << buffer << endl;

    // close the socket
    close(server_socket);
    return 0;
}